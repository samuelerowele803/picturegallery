package com.samuelHCB.photoGalleryApplication.dto;

import java.io.Serializable;


public record PictureDto(String name, String description) implements Serializable {
    @Override
    public String name() {
        return name;
    }

    @Override
    public String description() {
        return description;
    }
}
