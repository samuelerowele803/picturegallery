package com.samuelHCB.photoGalleryApplication.repository;

import com.samuelHCB.photoGalleryApplication.entity.Picture;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PictureRepository extends JpaRepository<Picture, Long> {
}