package com.samuelHCB.photoGalleryApplication.repository;

import com.samuelHCB.photoGalleryApplication.entity.Album;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AlbumRepository extends JpaRepository<Album, Long> {
}