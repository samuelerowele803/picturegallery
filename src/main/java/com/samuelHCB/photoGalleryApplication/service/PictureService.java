package com.samuelHCB.photoGalleryApplication.service;

import com.samuelHCB.photoGalleryApplication.dto.AlbumDto;
import com.samuelHCB.photoGalleryApplication.entity.Album;
import com.samuelHCB.photoGalleryApplication.entity.Picture;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Set;

public interface PictureService {

   Album createAlbum (AlbumDto albumDto);
   String addPicture ( MultipartFile file);
   String addPictureToAlbum(String albumName,String pictureName);
   String savePictureToAlbum(String albumName,MultipartFile file);
   List<Album> getAllAlbum ();
   AlbumDto findAlbumByName(String albumName);
   Set<Picture> getPictureFromAlbum(String albumName);
   List<Picture> getAllPicture();
   String deletePicture(String pictureName);
   String deleteAlbum (String albumName);
   String deletePictureFromAlbum(String albumName,String pictureName);
   Picture updatePicture (String pictureName,MultipartFile file);
   Album updatePictureFromAlbum(String albumName,String pictureName,MultipartFile file);

}
