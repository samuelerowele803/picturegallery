package com.samuelHCB.photoGalleryApplication.service;

import com.samuelHCB.photoGalleryApplication.entity.Album;
import com.samuelHCB.photoGalleryApplication.entity.Picture;
import com.samuelHCB.photoGalleryApplication.dto.AlbumDto;

import com.samuelHCB.photoGalleryApplication.repository.AlbumRepository;
import com.samuelHCB.photoGalleryApplication.repository.PictureRepository;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.time.LocalDate;
import java.util.Base64;
import java.util.List;
import java.util.Set;

@Service
public record PictureServiceImp(PictureRepository pictureRepository, AlbumRepository albumRepository, ModelMapper modelMapper)implements PictureService {
    @Override
    public Album createAlbum(AlbumDto albumDto) {
        Album album = new Album();
        album.setName(albumDto.name());
        album.setDescription(albumDto.description());
        album.setDateCreated(LocalDate.now());
        albumRepository.save(album);
        return album;
    }

    @Override
    public String addPicture( MultipartFile file) {
        Picture picture = new Picture();
        try {
            String pictureFile = Base64.getEncoder().encodeToString(file.getBytes());
            picture.setPicture(pictureFile);
            picture.setName(file.getOriginalFilename());
            picture.setType(file.getContentType());

            picture.setDateAdded(LocalDate.now());
            pictureRepository.save(picture);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        return null;
    }

    @Override
    public String addPictureToAlbum(String albumName, String pictureName) {
        Album album = albumRepository.findAll().stream().filter(a->a.getName().equalsIgnoreCase(albumName)).
                findAny().orElseThrow(()-> new RuntimeException("Album Not Found"));
        Picture picture = pictureRepository.findAll().stream().filter(p->p.getName().equalsIgnoreCase(pictureName)).
                findAny().orElseThrow(()-> new RuntimeException("picture Not Found"));
        picture.setAlbum(album);
        album.setDateModified(LocalDate.now());
        return null;
    }

    @Override
    public String savePictureToAlbum(String albumName,MultipartFile file) {
        Album album = albumRepository.findAll().stream().filter(a->a.getName().equalsIgnoreCase(albumName)).
                findAny().orElseThrow(()-> new RuntimeException("Album Not Found"));
        Picture picture = new Picture();

        try {
            String pictureFile = Base64.getEncoder().encodeToString(file.getBytes());
            picture.setName(file.getOriginalFilename());
            picture.setType(file.getContentType());
            picture.setDateAdded(LocalDate.now());
            picture.setPicture(pictureFile);
            picture.setAlbum(album);
            album.setDateModified(LocalDate.now());
            pictureRepository.save(picture);
            albumRepository.save(album);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        return null;
    }

    @Override
    public List<Album> getAllAlbum() {
       List<Album> albums = albumRepository.findAll();

        return albums;
    }

    @Override
    public AlbumDto findAlbumByName(String albumName) {

        Album album = albumRepository.findAll().stream().filter(a->a.getName().equalsIgnoreCase(albumName)).
                findAny().orElseThrow(()-> new RuntimeException("Album Not Found"));
        AlbumDto albumDto = new AlbumDto(album.getName(),album.getDescription());
        return albumDto;
    }

    @Override
    public Set<Picture> getPictureFromAlbum(String albumName) {
        Album album = albumRepository.findAll().stream().filter(a->a.getName().equalsIgnoreCase(albumName)).
                findAny().orElseThrow(()-> new RuntimeException("Album Not Found"));
        Set<Picture> picture = album.getPictures();
        return picture;
    }

    @Override
    public List<Picture> getAllPicture() {
        List<Picture> pictures = pictureRepository.findAll();

        return pictures;
    }

    @Override
    public String deletePicture(String pictureName) {
        Picture picture = pictureRepository.findAll().stream().filter(p->p.getName().equalsIgnoreCase(pictureName)).
                findAny().orElseThrow(()-> new RuntimeException("Picture Not Found"));
        pictureRepository.delete(picture);
        return null;
    }

    @Override
    public String deleteAlbum(String albumName) {
        Album album = albumRepository.findAll().stream().filter(a->a.getName().equalsIgnoreCase(albumName)).
                findAny().orElseThrow(()-> new RuntimeException("Album Not Found"));
        albumRepository.delete(album);
        return null;
    }

    @Override
    public String deletePictureFromAlbum(String albumName, String pictureName) {
        Album album = albumRepository.findAll().stream().filter(a->a.getName().equalsIgnoreCase(albumName)).
                findAny().orElseThrow(()-> new RuntimeException("Album Not Found"));
        Set<Picture> albumPictures = album.getPictures();
        for (Picture p:albumPictures) {
            if (p.getName().equalsIgnoreCase(pictureName)){
                albumPictures.remove(p);
            }else {
                throw  new RuntimeException("This Picture is Not In This Album");
            }
            albumRepository.save(album);
        }
        return null;
    }

    @Override
    public Picture updatePicture(String pictureName,MultipartFile file) {
        Picture picture = pictureRepository.findAll().stream().filter(a->a.getName().equalsIgnoreCase(pictureName)).
                findAny().orElseThrow(()-> new RuntimeException("Album Not Found"));
        if (file.getOriginalFilename()==null){picture.setName(picture.getName());
        }picture.setName(file.getOriginalFilename());
        if (file.getContentType()==null){picture.setType(picture.getType());
        }picture.setType(file.getContentType());
        if (file==null){picture.setPicture(picture.getPicture());
        }
        try {
            String pictureFile = Base64.getEncoder().encodeToString(file.getBytes());
            picture.setPicture(pictureFile);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        picture.setDateModified(LocalDate.now());
        pictureRepository.save(picture);

        return picture;
    }

    @Override
    public Album updatePictureFromAlbum(String albumName, String pictureName,MultipartFile file) {
        Album album = albumRepository.findAll().stream().filter(a-> a.getName().equalsIgnoreCase(albumName)).
                findAny().orElseThrow(()-> new RuntimeException("AlbumNot Found"));
        Set<Picture> albumPictures = album.getPictures();

        for (Picture p:albumPictures) {
            if (p.getName().equalsIgnoreCase(pictureName)){
                if (file.getOriginalFilename()==null){p.setName(p.getName());
                }p.setName(file.getOriginalFilename());
                if (file.getContentType()==null){p.setType(p.getType());
                }p.setType(file.getContentType());
                if (file==null){p.setPicture(p.getPicture());
                }
                try {
                    String pictureFile = Base64.getEncoder().encodeToString(file.getBytes());
                    p.setPicture(pictureFile);
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }

            }else {
                throw new RuntimeException("This Picture Is Not In This Album");
            }
            albumRepository.save(album);

        }


        return album;
    }
}
